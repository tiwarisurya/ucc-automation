package com.utilities;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Hashtable;
import org.testng.annotations.Test;

public class ReadData {
		
	//Get the current directory
	public static String getTestDataDirectory(String filename)
	{
		Path currentRelativePath = Paths.get("");
		String s = currentRelativePath.toAbsolutePath().toString();
		String workingpath = s +"\\src\\Resources\\"+filename;
		return(workingpath);
	}
}
