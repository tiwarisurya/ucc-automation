package com.chat;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import com.utilities.ReadData;
import com.utilities.TestData;

public class ChatFeature extends ReadData
{		
	@Test
	public void chatfeature () throws InterruptedException, FindFailed, IOException 
	{
        try
        {
    		TestData d = new TestData();
    		String username, password;
    		
    		//Create the object of screen
    		Screen screen = new Screen();		

    		//Declare variable
    		username = d.HostEmail;
    		password = d.Pswd;		

		//Close the application if running already
		Runtime.getRuntime().exec("taskkill /F /IM iMeet.exe");  
		Thread.sleep(2000);
		
		screen.doubleClick(screen.wait(getTestDataDirectory("Application.JPG"), 10));
		Thread.sleep(40000);
		//Login with first user
		
		//Enter email id
		screen.wait(getTestDataDirectory("EmailField.JPG"), 200);
		screen.type(getTestDataDirectory("EmailField.JPG"), "surya.tiwari@nagarro.com");
		Thread.sleep(8000);
		//Click on Continue
		screen.click(screen.wait(getTestDataDirectory("ContinueButton.JPG"), 10));
		Thread.sleep(8000);
		//Enter Password
		screen.type(getTestDataDirectory("PasswordField.JPG"), "myimeet123");
		Thread.sleep(8000);
		//Click on Sign In button
		screen.click(screen.wait(getTestDataDirectory("SignInButton.JPG"), 10));
		Thread.sleep(8000);
		//Click on Chat
		screen.click(screen.wait(getTestDataDirectory("Chat.JPG"), 10));
		Thread.sleep(8000);
		
		//Click on New Chat
		screen.click(screen.wait(getTestDataDirectory("NewChat.JPG"), 10));
		Thread.sleep(8000);
		
		//Enter 2nd users email id
		screen.type(getTestDataDirectory("ChatUserEmailField.JPG"), "test.user@test.com");
		Thread.sleep(8000);
		
		//Click on Auto suggested user from drop down
		screen.click(screen.wait(getTestDataDirectory("UserInDropDown.JPG"), 10));
		Thread.sleep(8000);
		
		//Enter chat message. 
		screen.type(getTestDataDirectory("MessageField_TextBox.JPG"), "Hello This is Test Message.");				
		Thread.sleep(8000);
		
		//Click on Send button
		screen.click(screen.wait(getTestDataDirectory("StartButton.JPG"), 10));
		Thread.sleep(8000);
		
		
		//Click on Settings icon
		Thread.sleep(8000);
		screen.click(screen.wait(getTestDataDirectory("Setting.JPG"), 10));
		Thread.sleep(8000);
		//Click on Sign out
		screen.click(screen.wait(getTestDataDirectory("SignOut.JPG"), 10));
		Thread.sleep(15000);
		
		//Enter email id
		screen.wait(getTestDataDirectory("EmailField.JPG"), 200);
		screen.type(getTestDataDirectory("EmailField.JPG"), "surya.tiwari@nagarro.com");
		Thread.sleep(8000);
		//Click on Continue
		screen.click(screen.wait(getTestDataDirectory("ContinueButton.JPG"), 10));
		Thread.sleep(8000);
		//Enter Password
		screen.type(getTestDataDirectory("PasswordField.JPG"), "myimeet123");
		Thread.sleep(8000);
		//Click on Sign In button
		screen.click(screen.wait(getTestDataDirectory("SignInButton.JPG"), 10));
		Thread.sleep(8000);
		//Click on Chat tab
		screen.click(screen.wait(getTestDataDirectory("Chat.JPG"), 10));
		Thread.sleep(8000);
		
		//Click on Chat
		screen.click(screen.wait(getTestDataDirectory("User1SuryaTiwari.JPG"), 10));
		Thread.sleep(8000);
		
		//Verify Received Chat
		if (screen.exists(getTestDataDirectory("ReceivedChat.JPG")) != null)
		{
		System.out.print ("Image verified successfully");
		}
		else
		{
		System.out.print ("Expected Image, Start Image, not found");
		}
		
		//Click on Settings icon
		Thread.sleep(8000);
		screen.click(screen.wait(getTestDataDirectory("Setting.JPG"), 10));
		Thread.sleep(8000);
		
		//Click on Sign out
		screen.click(screen.wait(getTestDataDirectory("SignOut.JPG"), 10));
		Thread.sleep(15000);
		
   }
	catch (Throwable t)
	{
		t.getMessage();
	}
        
        //Close the application.
		Runtime.getRuntime().exec("taskkill /F /IM iMeet.exe"); 
}
}