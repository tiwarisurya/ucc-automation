package com.meetings;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;
import org.sikuli.script.SikuliException;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.utilities.ReadData;
import com.utilities.TestData;


public class IM_25441_Meetings_001 extends ReadData 
{		
	//Create the object of screen
	Screen screen = new Screen();	
	SoftAssert s_assert = new SoftAssert();
	
	@Test //IM_25441_Meetings_001_Layout of Meetings screen for Host user.
	public void startMeeting () throws InterruptedException, FindFailed, IOException 
	{
        try
        {
    		TestData d = new TestData();
    		String username, password;	

    		//Declare varriable
    		username = d.HostEmail;
    		password = d.Pswd;

		//Close the application if running already
		Runtime.getRuntime().exec("taskkill /F /IM iMeet.exe");  
		Thread.sleep(12000);
		
		screen.doubleClick(screen.wait(getTestDataDirectory("Application.JPG"), 10));
		Thread.sleep(30000);
		//Enter email id
		screen.wait(getTestDataDirectory("EmailField.JPG"), 200);
		screen.type(getTestDataDirectory("EmailField.JPG"), "surya.tiwari@nagarro.com");
		Thread.sleep(8000);
		//Click on Continue
		screen.click(screen.wait(getTestDataDirectory("ContinueButton.JPG"), 10));
		Thread.sleep(8000);
		//Enter Password
		screen.type(getTestDataDirectory("PasswordField.JPG"), "myimeet123");
		Thread.sleep(8000);
		//Click on Sign In button
		screen.click(screen.wait(getTestDataDirectory("SignInButton.JPG"), 10));
		Thread.sleep(8000);
		//Click on Meeting tab
		screen.click(screen.wait(getTestDataDirectory("Meetings.JPG"), 10));
		Thread.sleep(8000);
				
		//verify the host user in meeting room.
		Assert.assertTrue((screen.exists(getTestDataDirectory("StartMeeting.JPG")) != null), "Image not found");
		if (screen.exists(getTestDataDirectory("StartMeeting.JPG")) != null)
		{
		System.out.print ("Image verified successfully");
		}
		else
		{
		System.out.print ("Expected Image, Start Image, not found");
		}
		
		Thread.sleep(8000);
		//Click on Settings icon
		screen.click(screen.wait(getTestDataDirectory("Setting.JPG"), 10));
		Thread.sleep(8000);
		//Click on Sign out
		screen.click(screen.wait(getTestDataDirectory("SignOut.JPG"), 10));
		Thread.sleep(15000);
   }
	catch (Throwable t)
	{
		s_assert.fail("Error Occured . Detail " + t.getMessage());
	}
        
        //Close the application.
		Runtime.getRuntime().exec("taskkill /F /IM iMeet.exe"); 
}
}