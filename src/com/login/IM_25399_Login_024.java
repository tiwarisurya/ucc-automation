package com.login;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;
import org.testng.annotations.Test;

import com.utilities.ReadData;
import com.utilities.TestData;

public class IM_25399_Login_024 extends ReadData
{		
	@Test
	public void loginintoApplication () throws InterruptedException, FindFailed, IOException 
	{
        try
        {
    		TestData d = new TestData();
    		String username, password;
    		
    		//Create the object of screen
    		Screen screen = new Screen();		

    		//Declare varriable
    		username = d.HostEmail;
    		password = d.Pswd;		

		//Close the application if running already
		Runtime.getRuntime().exec("taskkill /F /IM iMeet.exe");  
		Thread.sleep(2000);
		
		screen.doubleClick(screen.wait(getTestDataDirectory("Application.JPG"), 10));
		Thread.sleep(40000);
		//Enter email id
		screen.wait(getTestDataDirectory("EmailField.JPG"), 200);
		screen.type(getTestDataDirectory("EmailField.JPG"), "surya.tiwari@nagarro.com");
		Thread.sleep(8000);
		//Click on Continue
		screen.click(screen.wait(getTestDataDirectory("ContinueButton.JPG"), 10));
		Thread.sleep(8000);
		//Enter Password
		screen.type(getTestDataDirectory("PasswordField.JPG"), "myimeet123");
		Thread.sleep(8000);
		//Click on Sign In button
		screen.click(screen.wait(getTestDataDirectory("SignInButton.JPG"), 10));
		Thread.sleep(8000);
		//Click on Settings icon
		screen.click(screen.wait(getTestDataDirectory("Setting.JPG"), 10));
		Thread.sleep(8000);
		//Click on Sign out
		screen.click(screen.wait(getTestDataDirectory("SignOut.JPG"), 10));
		Thread.sleep(15000);
   }
	catch (Throwable t)
	{
		t.getMessage();
	}
        
        //Close the application.
		Runtime.getRuntime().exec("taskkill /F /IM iMeet.exe"); 
}
}